#pragma	once

#include <string>
#include <vector>
#include <glm/glm.hpp>
#include <iostream>
#include <SDL2/SDL_mixer.h>

//#include "ScreenObject.h"
#include "playerObject.h"
#include "ghostObject.h"

struct tile
{
	bool open;
	bool closed;
	int h, f ,g;
	glm::vec2 pos;
	tile* lastTile;
};

class Level
{
public:
	Level(std::string filepath);
	void update(float dt);

	std::vector<ScreenObject>* getWalls();
	std::vector<ScreenObject>* getOrbs();
	std::vector<ghostObject>* getGhosts();
	std::vector<ScreenObject>* getPowerUps();
	playerObject* getPlayer();
	void pickedup(int y, int x);
	bool nextLevel();
	bool hit();
	glm::vec2 astar(glm::vec2 startPos, glm::vec2 endPos, glm::vec2 dir);

private:
	const int wall = 1;
	const int orb = 3;
	const int playerint = 2;
	const int ghost = 4;
	const int powerUp = 5;
	const int playerwall = 9;
	const int bigNumber = 100000;

	std::vector<std::vector<int>> map;
	std::vector<std::vector<tile>> tileMap;
	std::vector<ScreenObject> walls;
	std::vector<ScreenObject> orbs;
	std::vector<ScreenObject> powerUps;
	std::vector<ghostObject> ghosts;
	playerObject player;
	glm::vec2 size;

	bool isItPOWERTIME;

	float time, starttime, powerTime;

	Mix_Music* lavenderTown;

	bool collision(float deltaTime);

	void loadMap(std::string filepath); //Loads map data from file
	void createMap();	//This should create a ScreenObject for each wall segment.  
	void openNeighbor(glm::vec2 offset, tile currentTile, glm::vec2 endPos);
	void SmallestOpenTile(tile &currentTile);
	void activatePowerUp(int x, int y);
	
};