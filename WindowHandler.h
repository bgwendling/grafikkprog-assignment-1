#pragma once

#include <SDL2/SDL.h>
#include <vector>
#include <glm/glm.hpp>
#include <SDL2/SDL_image.h>

#include "textHandler.h"
#include "ScreenObject.h"
#include "ghostObject.h"

class WindowHandler {
public:
	//This ensures that there can only be one WindowHandler object at any given time.
	static WindowHandler& getInstance() {
		static WindowHandler instance;
		return instance;
	}

	bool init();
	void clear();//Clears the back buffer, making it ready for a new frame.
	//Loops through a list of ScreenObjects and calls the draw function on them.
	//This is used to prevent exessive calling of getInstance()
	void drawList(std::vector<ScreenObject>* objects);
	void drawList(std::vector<ghostObject>* ghosts);
	void draw(ScreenObject* object); //This should draw a ScreenObject object or a child class there of.
	void drawText();
	void update(); //Switches the front and back buffer
	void close();
	void reset();

	SDL_Surface* getSurface();
	glm::vec2 getScreenSize();
	textHandler* getTextHandlerScore();
	textHandler* getTextHandlerLife();
	//This ensures that there can only be one WindowHandler object at any given time.
	WindowHandler(WindowHandler const& copy) = delete;
	void operator=(WindowHandler const& copy) = delete;
private:
	SDL_Window* window = nullptr;
	SDL_Surface* screenSurface = nullptr;
	SDL_Renderer* renderer = nullptr;
	textHandler* textHandlerScore = nullptr;
	textHandler* textHandlerLife = nullptr;

	char windowName[100];
	int windowXSize;
	int windowYSize;

	const int fontSize = 20;
	const int fontStartY = 10;
	const int fontScoreStartX = 0;
	const int fontLifeStartX = 350;

	std::vector<SDL_Texture*> spriteSheet;

	WindowHandler() {};
	bool initSDL();
	void loadConfig();
};