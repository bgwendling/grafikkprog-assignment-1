#pragma once

#include <glm/glm.hpp>
#include "ScreenObject.h"
#include "WindowHandler.h"


class playerObject : public ScreenObject
{
public:
	playerObject() {};
	playerObject(glm::vec2 pos, glm::vec2 size); 

	void direction(glm::vec2 direction);
	void update(float dt, bool powerTime);
	glm::vec2 getVel();
	void setVelocity(glm::vec2 dir);
	glm::vec2 getDir();
	void reset(); 
private:
	glm::vec2 vel;
	glm::vec2 dir; 
	float speed;
	float time;
	int frame;
	bool increasing;

	
};