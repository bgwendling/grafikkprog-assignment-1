#include "Level.h"
#include "WindowHandler.h"

Level::Level(std::string filepath) {
	loadMap(filepath);
	createMap();
	lavenderTown = Mix_LoadMUS("lavenderTown.mp3");
}

/* 
Reads a text file containing map data in the following format:
5x5
1 1 1 1 1
1 0 0 0 1
2 0 1 0 0
1 0 0 0 1
1 1 1 1 1
The first line defines the dimensions of the map
The following matrix has 1s representing walls and 0s representing open space.
2 represents Pac Mans start position.
*/
void Level::loadMap(std::string filepath) {
	int x, y, temp;
	FILE* file = fopen(filepath.c_str(), "r");
	
	fscanf(file, "%dx%d", &x, &y);

	for(int i = 0; i<y; i++) {
		std::vector<int> row;
		for(int j = 0; j<x; j++) {
			fscanf(file, " %d", &temp);
			row.push_back(temp);
		}
		map.push_back(row);
	}

	fclose(file);
	file = nullptr;
}

//Uses the map data to create ScreenObjects that represent the walls of the map.
void Level::createMap() {
	glm::vec2 pos;
	int rows = map.size();
	int cells = 0;
	const float sizeOffset = 1.0f;	// for collision on packman and ghost
	const int bigNumber = 10000;	// for h value for astar only used on walls
	isItPOWERTIME = false;
	powerTime = 0;

	time = 0;
	starttime = 0;
	size = WindowHandler::getInstance().getScreenSize();
	size.x /=  map[0].size();
	size.y /= map.size();

	for(int i = 0; i<rows; i++) {
		cells = map[i].size();
		tileMap.push_back(std::vector<tile>());

		for(int j = 0; j<cells; j++) {
			pos.x = j * size.x;
			pos.y = i * size.y;
			tile temp;
			if(map[i][j] == wall) {

				temp.h = bigNumber;
				ScreenObject wall(pos, size, glm::vec2(4, 5));
				walls.push_back(wall);
			}
			else
			{
				temp.h = 1; // h value for astar is the same for all but walls
			}
			if(map[i][j] == playerwall)
			{
				ScreenObject wall(pos, size, glm::vec2(4, 7));
				walls.push_back(wall);
			}
			if(map[i][j] == playerint)
			{

				//Collision detection works on principle that pacman and ghosts is slightly smaller then
				//the walls. Hence, the -1.0f
				
				player = playerObject(pos, size - sizeOffset);
			}
			if (map[i][j] == orb)
			{
				ScreenObject orb(pos, size, glm::vec2(4,4));
				orbs.push_back(orb);
			}
			if (map[i][j] == ghost)
			{
				ghostObject ghost(pos, size - sizeOffset, ghosts.size());
				ghosts.push_back(ghost);	
			}
			if(map[i][j] == powerUp)
			{
				ScreenObject apowerup(pos, size, glm::vec2(4, 0));
				powerUps.push_back(apowerup);
			}


			temp.open = false;		// creates a tile for astar with the x,y cordinates
			temp.closed = false;
			temp.pos.x = (float)j;
			temp.pos.y = (float)i;
			temp.lastTile = nullptr;
			tileMap[i].push_back(temp);
		}
	}
}

std::vector<ScreenObject>* Level::getWalls() 
{
	return &walls;
}

playerObject* Level::getPlayer()
{
	return &player;
}

std::vector<ScreenObject>* Level::getOrbs()
{
	return &orbs;
}
std::vector<ScreenObject>* Level::getPowerUps()
{
	return &powerUps;
}
std::vector<ghostObject>* Level::getGhosts()
{
	return &ghosts;
}
bool Level::nextLevel()
{
	return (!(orbs.size())); 
}
bool Level::hit()
{
	const int hitbox = 20;	//how close you have to be to hit
	const int score = 100;
	for(uint i = 0; i < ghosts.size(); ++i)
	{
		
		glm::vec2 dist = player.getPosition() - ghosts[i].getPosition();
		if(sqrt(dist.x * dist.x + dist.y * dist.y) < hitbox)
		{
			if (isItPOWERTIME)
			{
				WindowHandler::getInstance().getTextHandlerScore()->updateScore(score);
				ghosts[i].reset();
			}
			else
			{
				if(WindowHandler::getInstance().getTextHandlerLife()->updatelife())
				{
					return true;
				}
				else
				{
					player.reset();
					for (uint j = 0; j < ghosts.size(); ++j)
					{
						ghosts[j].reset();
						starttime = 0;
						time = 0;
					}
				}
			}
		}
	}
	return false;
}

void Level::update(float dt)
{	

	const int maxPowerTime = 5;

	if (!collision(dt))
	{
		player.update(dt, isItPOWERTIME);
	}
 	const int timeGoal = 7;
 	const int resetstimeGoal = 8;
	time += dt;			
	starttime += dt;

	if (Mix_PlayingMusic() == 0)
	{
		Mix_PlayMusic(lavenderTown, -1);
	}
	if (isItPOWERTIME)
	{
		powerTime += dt;
		if (powerTime >= maxPowerTime)
		{
			 isItPOWERTIME = false;
		}
	}

	for (size_t i = 0; i < ghosts.size(); ++i)
	{
		bool up = false;
		bool down = false;
		bool left = false;
		bool right = false;
		glm::vec2 astarresult = glm::vec2(0,0);

		if((((int)ghosts[i].getPosition().x % (int)size.x) <
	 	(int)(ghosts[i].getPosition().x + ghosts[i].getDestRect().w)  % (int)size.x)
		&& ((int)ghosts[i].getPosition().y % (int)size.y <
		(int)(ghosts[i].getPosition().y + ghosts[i].getDestRect().h) % (int)size.y))
		{
			if(ghosts[i].getChangedTheFuckingThing())
			{
				glm::vec2 tilePos = ghosts[i].getPosition()/ size;

				if(map[(int)tilePos.y][(int)tilePos.x + 1] != wall)
				{
					right = true;
				}
				
				if(map[(int)tilePos.y][(int)tilePos.x - 1] != wall)
				{
					left = true;
				}
				
				if(map[(int)tilePos.y + 1][(int)tilePos.x] != wall)
				{
					down = true;
				}
				
				if(map[(int)tilePos.y - 1][(int)tilePos.x] != wall)
				{
					up = true;
				}
				if (right + left + down + up > 2) // checks if the ghosts can go in 3 or more ways
				{
					glm::vec2 endpos = player.getPosition();
					if (time > timeGoal || starttime < timeGoal || isItPOWERTIME) 
					{
						
						endpos.x = size.x * 3 + size.x * 24.0f * (i % 2);
						endpos.y = size.y * 4.0f + size.y * 28.0f * (i / 2);
					}
					if (time > resetstimeGoal)
					{
						time = 0;
					}
					astarresult = astar(ghosts[i].getPosition(), endpos , ghosts[i].getDir());
				}

				ghosts[i].setTheFuckingThing(false);			
			}
		}
		else
		{
			ghosts[i].setTheFuckingThing(true);
		}
		ghosts[i].update(dt, up, down, left, right, astarresult);	
	}
}

bool Level::collision(float deltaTime)
{
	glm::vec2 playerPos;

	playerPos = (player.getPosition() + (size /2.0f))/size;
	if (map[(int)playerPos.y][(int)playerPos.x] == orb)
	{
		pickedup((int)playerPos.y, (int)playerPos.x);
	}
	if (map[(int)playerPos.y][(int)playerPos.x] == powerUp)
	{
		activatePowerUp((int)playerPos.x, (int)playerPos.y) ;
	}
	if((((int)player.getPosition().x % (int)size.x) <
	  (int)(player.getPosition().x + player.getDestRect().w)  % (int)size.x)
	  && ((int)player.getPosition().y % (int)size.y <
	  (int)(player.getPosition().y + player.getDestRect().h) % (int)size.y))
	{
		glm::vec2 tempMapPos;
		glm::vec2 tempTempTemp;
		tempMapPos = (player.getPosition() / size) + player.getDir();
		tempTempTemp = (player.getPosition() / size) + glm::normalize(player.getVel());
		

		if(map[(int)tempMapPos.y][(int)tempMapPos.x] != 1 && map[(int)tempMapPos.y][(int)tempMapPos.x] != 9)
		{

			player.setVelocity(player.getDir());
		}else if(map[tempTempTemp.y][tempTempTemp.x] == wall)
		{

			player.setVelocity(glm::vec2(0.0f, 0.0f));
			return true;
		}
	}


	return false;
}

void Level::pickedup(int y, int x)
{
	glm::vec2 pos;

	pos.x = x * size.x;
	pos.y = y * size.y;
	uint score;

	for (u_int i = 0; i < orbs.size(); ++i)
	{
		if(orbs[i].getPosition() == pos)
		{
			map[y][x] = 0;
			orbs.erase(orbs.begin() + i);	
			score = 15;
		}
	}
	WindowHandler::getInstance().getTextHandlerScore()->updateScore(score);
}
glm::vec2 Level::astar(glm::vec2 startPos, glm::vec2 endPos, glm::vec2 startDir)
{	
glm::vec2 dir = glm::vec2(0, 0), tileStartPos, tileEndPos;

	const int somenumber = 100;
	const int lastTilenumberY = tileMap.size() - 1;
	const int lastTilenumberX = tileMap[0].size() -1; 

	tile currentTile;
	tileStartPos = startPos / size; 
	tileEndPos = endPos/ size;
	tileMap[tileStartPos.y - startDir.y][tileStartPos.x - startDir.x].h = somenumber;
	for (uint i = 0; i < tileMap.size(); ++i)
	{
		for (uint j = 0; j < tileMap[i].size(); ++j)
		{
			tileMap[i][j].g = bigNumber;
			tileMap[i][j].f = tileMap[i][j].h + tileMap[i][j].g;
			tileMap[i][j].open = false;
			tileMap[i][j].closed = false;
			tileMap[i][j].lastTile = nullptr;
		}
	}

	tileMap[tileStartPos.y][tileStartPos.x].open = true;
	currentTile.pos = tileStartPos;

	while(((int)currentTile.pos.x != (int)tileEndPos.x) || ((int)currentTile.pos.y != (int)tileEndPos.y))
	{
		SmallestOpenTile(currentTile);

		tileMap[currentTile.pos.y][currentTile.pos.x].open = false;
		tileMap[currentTile.pos.y][currentTile.pos.x].closed = true;

		glm::vec2 temp = glm::vec2(0.0f,0.0f);
		temp.x = 1;
		temp.y = 0;
		if(currentTile.pos.x >= lastTilenumberX)
		{
		 	temp.x = -lastTilenumberX;
		} 
		openNeighbor(temp, currentTile, tileEndPos);

		temp.x = -1; 
		temp.y = 0;
		if(currentTile.pos.x <= 0)
		{
			temp.x = lastTilenumberX;
		}
		openNeighbor(temp, currentTile, tileEndPos);

		temp.y = 1;
		temp.x = 0;
		if(currentTile.pos.y >= lastTilenumberY)
		{
			temp.y = -lastTilenumberY;
		}
		openNeighbor(temp, currentTile, tileEndPos);

		temp.y = -1; 
		temp.x = 0;
		if(currentTile.pos.y <= 0)
		{
			temp.y = lastTilenumberY;
		}
		openNeighbor(temp, currentTile, tileEndPos);
			

	}

	tile* tempTemp = &tileMap[currentTile.pos.y][currentTile.pos.x];
	while(tempTemp->lastTile != nullptr)
	{
		dir.x = tempTemp->pos.x - (int)tileStartPos.x;
		dir.y = tempTemp->pos.y - (int)tileStartPos.y;
		tempTemp = tempTemp->lastTile;
	}

 	tileMap[tileStartPos.y - startDir.y][tileStartPos.x - startDir.x].h = 1;

	return dir;
}
	


void Level::openNeighbor(glm::vec2 offset, tile currentTile, glm::vec2 endPos)
{
	glm::vec2 sidetile = offset + currentTile.pos;
	if(sidetile.x >= 0 && sidetile.x < tileMap[0].size() && sidetile.y >= 0 && sidetile.y < tileMap.size())
	{
		float newg = tileMap[sidetile.y][sidetile.x].h + currentTile.g;
 		if (!tileMap[sidetile.y][sidetile.x].closed)
		{	
			if (!tileMap[sidetile.y][sidetile.x].open)
			{
				tileMap[sidetile.y][sidetile.x].open = true;
			}
			if(tileMap[sidetile.y][sidetile.x].g > newg )	
			{
				tileMap[sidetile.y][sidetile.x].g = newg;
				tileMap[sidetile.y][sidetile.x].lastTile = &tileMap[currentTile.pos.y][currentTile.pos.x];
				glm::vec2 difference = sidetile - endPos; 
				tileMap[sidetile.y][sidetile.x].f = tileMap[sidetile.y][sidetile.x].h + tileMap[sidetile.y][sidetile.x].g 
				+ sqrt(difference.x * difference.x + difference.y * difference.y);
			}
		}
	}
}
void Level::SmallestOpenTile(tile &currentTile)
{
	currentTile.f = bigNumber;

	for (uint i = 0; i < tileMap.size(); ++i)
	{
		for (uint j = 0; j < tileMap[i].size(); ++j)
		{
			if (tileMap[i][j].open && !tileMap[i][j].closed)
			{
				if (currentTile.f > tileMap[i][j].f)
				{
					currentTile = tileMap[i][j];
				}	
			}
		}
	}
}
void Level::activatePowerUp(int x, int y)
{	
	
	glm::vec2 position;
	position.x = x * size.x;
	position.y = y * size.y;
	for (u_int i = 0; i < powerUps.size(); ++i)
	{
		if(powerUps[i].getPosition() == position)
		{
			map[y][x] = 0;
			powerUps.erase(powerUps.begin() + i);

			isItPOWERTIME = true;
			powerTime = 0;

		}
	}
}