#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
#include <vector>
#include <iostream>

class textHandler
{
public:
	textHandler(SDL_Renderer* renderer);
	~textHandler();

	void updateScore(int increment);
	bool updatelife();

	std::vector<SDL_Rect>& getRectangles();
	SDL_Texture* getFont();

private:
	int score;
	int life;
	SDL_Texture* font;
	std::string fontString, currentString, oldString;
	std::vector<SDL_Rect> rectangles;

	SDL_Rect getSourceRect(char character);
	void updatetext();
};