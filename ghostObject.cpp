#include "ghostObject.h"

ghostObject::ghostObject(glm::vec2 pos, glm::vec2 size, int t) : ScreenObject(pos, size, glm::vec2(0,4))
{
	type = t;
	speed = 99.0f; // ghost speed is to damn high
	time = 0;
	vel = glm::vec2(1, 0) * speed;

}

ghostObject::~ghostObject()
{

}

void ghostObject::update(float dt, bool up, bool down, bool left, bool right, glm::vec2 astarresult)
{
	
	const int spriteSize = 256 / 4;


	updateVel(up, down, left, right, astarresult);

	position += vel*dt;

 	if(vel.x == speed)
 	{
 		source.y = 6*spriteSize;
 	}
 	else if(vel.x == -speed)
 	{
 		source.y = 5*spriteSize;
 	}
  	if(vel.y == speed)
 	{
 		source.y = 4*spriteSize;
 	}
 	else if(vel.y == -speed)
 	{
 		source.y = 7*spriteSize;
 	}
 	
 	time += dt ;
 	
 	if (time >= 0.2)
 	{
 		++frame;

 		time = 0;
 		if (frame == 3)
 		{
 			frame = 0;
 		}
 	}

 	source.x = frame* spriteSize;		

 	if(position.x < 0)			
 	{
 		position.x =  710 ;
 	}
 	else if(position.x > 710)
 	{
 		position.x = 10;
 	}
 	else if(position.y < 40 )
 	{
 		position.y = 690;
 	}
 	else if(position.y > 690)
 	{
 		position.y = 40 ;
 	}
}

void ghostObject::updateVel(bool up, bool down, bool left, bool right, glm::vec2 astarresult)
{
	glm::vec2 newVel = glm::vec2(0, 0);
	
	if ((int)up + (int)down +(int)left + (int)right < 3)
	{	
		if(left)
		{
			glm::vec2 temp = glm::vec2(-1, 0);
			if(temp !=  -glm::normalize(vel))
			{
				newVel = temp * speed;
			}	
		}
		if(right)
		{
			glm::vec2 temp = glm::vec2(1, 0);
			if(temp != -glm::normalize(vel))
			{
				newVel = temp * speed;
			}
		}
		if(up)
		{
			glm::vec2 temp = glm::vec2(0, -1);
			if(temp != -glm::normalize(vel))
			{
				newVel = temp * speed;
			}
		}
		if(down)
		{
			glm::vec2 temp = glm::vec2(0, 1);
			if(temp != -glm::normalize(vel))
			{
				newVel = temp * speed;
			}
		}
		if(newVel.x != 0 || newVel.y != 0)
		{
			vel = newVel;
		}
	}
	else
	{
		if(astarresult.x != 0.0f || astarresult.y != 0.0f)
		{
			vel = astarresult * speed;
		}
	}	


}
void ghostObject::reset()
{
	position = startPosition;
	vel = glm::vec2(1,0) * speed;

}