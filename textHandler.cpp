#include "textHandler.h"

textHandler::textHandler(SDL_Renderer* renderer)
{
	//These are all the common letters we might need
	fontString = "!\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~ ";

	SDL_Surface* loadedFont = IMG_Load("Arial.bmp");
	font = SDL_CreateTextureFromSurface(renderer, loadedFont);
	SDL_FreeSurface(loadedFont);
	score = 0;

	//(Actual life + 1) need to call updatelife to display lives in the beginning of the map
	life = 4;
	currentString = "";
}

textHandler::~textHandler()
{

}
void textHandler::updateScore(int increment)
{
	score += increment;

	oldString = currentString;
	currentString = "Score: " + std::to_string(score);
	updatetext();
}
bool textHandler::updatelife()
{
	life--;
	oldString = currentString;
	currentString = "Lives: " + std::to_string(life);
	updatetext();

	if(life <= 0)
	{
		return true;
	}

	return false;
}
void textHandler::updatetext()
{


	while (rectangles.size() < currentString.length())
	{
		SDL_Rect wasd;
		wasd.x = 0;
		wasd.y = 0;
		wasd.w = 21;
		wasd.h = 21;
		rectangles.push_back(wasd);
		oldString += "*";
	}

	for (uint i = 0; i < currentString.length(); ++i)
	{
		if (currentString[i] != oldString[i])
		{
			rectangles[i] = getSourceRect(currentString[i]);
		}
	}
}

std::vector<SDL_Rect>& textHandler::getRectangles()
{
	return rectangles;
}

SDL_Texture* textHandler::getFont()
{
	return font;
}

SDL_Rect textHandler::getSourceRect(char character)
{
	SDL_Rect temp;
	int width;
	int height;
	SDL_QueryTexture(font, NULL, NULL, &width, &height);

	const int charPerLine = 20;
	const int noOfLines = 10;

	for(uint i = 0; i < fontString.length(); ++i)
	{
		if(fontString[i] == character)
		{
			temp.x = (i % charPerLine) * (width / charPerLine);
			temp.y = (i / charPerLine) * (height / noOfLines);

			temp.w = (width / charPerLine);
			temp.h = (height/ noOfLines);

			return temp;
		}
	}

	//Should never be called
	SDL_Rect wasd;
	wasd.x = 0;
	wasd.y = 0;
	wasd.w = 21;
	wasd.h = 21;
	return wasd;
}