#include "playerObject.h"



playerObject::playerObject(glm::vec2 pos, glm::vec2 size) : ScreenObject(pos, size, glm::vec2(0,0))
{
	speed = 100;
	vel.x = speed;
	vel.y = 0;
	time = 0;
	frame = 0;
	increasing = true;
	dir = glm::vec2(1,0);
}

void playerObject::direction(glm::vec2 direction) 
{
	if(glm::dot(direction, glm::normalize(vel)) == -1)
	{
		vel = -vel;
	}
	else
	{
		dir = direction;
	}
}


void playerObject::update(float dt, bool powerTime)
{
	const int spriteSize = 256 / 4;
	int frameoffset = 0;

	position += vel*dt;

 	if(vel.x == speed)
 	{
 		source.y = 2*spriteSize;
 	}
 	else if(vel.x == -speed)
 	{
 		source.y = 1*spriteSize;
 	}
  	if(vel.y == speed)
 	{
 		source.y = 0;
 	}
 	else if(vel.y == -speed)
 	{
 		source.y = 3*spriteSize;
 	}
 	time += dt ;
 	if (time >= 0.07 )
 	{
 		++frame;

 		time = 0;
 		if (frame == 3)
 		{
 			frame = 0;
 		}
 	}
 	if (powerTime)
	{
		frameoffset = 4;
	}
 	source.x = (frame + frameoffset)* spriteSize;

 	if(position.x <= 0 )
 	{
 		position.x =  WindowHandler::getInstance().getScreenSize().x - 1;
 	}
 	else if(position.x >= WindowHandler::getInstance().getScreenSize().x)
 	{
 		position.x = 1 ;//- size.x;
 	}
 	else if(position.y <= 40)
 	{
 		position.y = WindowHandler::getInstance().getScreenSize().y - size.x;
 	}
 	else if(position.y > WindowHandler::getInstance().getScreenSize().y - size.x)
 	{
 		position.y = 41;
 	}
}

glm::vec2 playerObject::getVel()
{
	return vel;
}

void playerObject::setVelocity(glm::vec2 dir)
{
	vel = dir * speed;
}

glm::vec2 playerObject::getDir()
{
	return dir;
}
void playerObject::reset()
{
	position = startPosition;
	vel = glm::vec2(1,0) * speed;
	dir = glm::vec2(1,0);
}
