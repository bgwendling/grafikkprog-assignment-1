#include "menuScreen.h"

menuScreen::menuScreen()
{
	glm::vec2 buttonSize = glm::vec2(200,100);
	
	glm::vec2 startButtonPos = WindowHandler::getInstance().getScreenSize();
	startButtonPos /= 2.0f;
	startButtonPos.x -= buttonSize.x/2;
	startButtonPos.y -= (buttonSize.y/2) + 100;

	glm::vec2 quitButtonPos = WindowHandler::getInstance().getScreenSize();
	quitButtonPos /= 2.0f;
	quitButtonPos.x -= buttonSize.x/2;
	quitButtonPos.y -= (buttonSize.y/2) - 100;

	startButton = ScreenObject(startButtonPos, buttonSize, glm::vec2(0,512), buttonSize);
	quitButton = ScreenObject(quitButtonPos, buttonSize, glm::vec2(200,512), buttonSize);
}

bool menuScreen::update(glm::vec2 mPos)
{
	SDL_Rect startButtonRect = startButton.getDestRect();
	SDL_Rect quitButtonRect = quitButton.getDestRect();

	if (mPos.x >= startButtonRect.x && mPos.x <= startButtonRect.x + startButtonRect.w &&
		mPos.y >= startButtonRect.y && mPos.y <= startButtonRect.y + startButtonRect.h)
	{

		return false;
	}

	if(mPos.x >= quitButtonRect.x && mPos.x <= quitButtonRect.x + quitButtonRect.w &&
		mPos.y >= quitButtonRect.y && mPos.y <= quitButtonRect.y + quitButtonRect.h)
	{
		gRunning = false;
	}
	return true;
}

ScreenObject* menuScreen::getStartButton()
{
	return &startButton;
}

ScreenObject* menuScreen::getQuitButton()
{
	return &quitButton;
}