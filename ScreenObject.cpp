#include "ScreenObject.h"
ScreenObject::ScreenObject(glm::vec2 pos, glm::vec2 sz, glm::vec2 spriteLocation)
{
	const int spriteSize = 256 / 4;

	position = pos;
	startPosition = pos;
	size = sz;
	source.x = spriteSize * (int)spriteLocation.x;
	source.y = spriteSize * (int)spriteLocation.y;
	source.w = spriteSize;
	source.h = spriteSize;
}

ScreenObject::ScreenObject(glm::vec2 pos, glm::vec2 sz, glm::vec2 spriteLocation, glm::vec2 spriteSize)
{
	position = pos;
	startPosition = pos;
	size = sz;
	source.x = spriteLocation.x;
	source.y = spriteLocation.y;
	source.w = spriteSize.x;
	source.h = spriteSize.y;
}

glm::vec2 ScreenObject::getPosition() 
{
	return position;
}

SDL_Rect ScreenObject::getSourceRect() 
{
	return source;
}

SDL_Rect ScreenObject::getDestRect() 
{
	SDL_Rect rect;

	rect.x = static_cast<int>(position.x);
	rect.y = static_cast<int>(position.y);
	rect.w = static_cast<int>(size.x);
	rect.h = static_cast<int>(size.y);

	return rect;
}
