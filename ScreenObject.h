#pragma	once

#include <SDL2/SDL.h>
#include <glm/glm.hpp>

class ScreenObject {
public:
	ScreenObject() {};
	ScreenObject(glm::vec2 pos, glm::vec2 sz, glm::vec2 spriteLocation);
	ScreenObject(glm::vec2 pos, glm::vec2 sz, glm::vec2 spriteLocation, glm::vec2 spriteSize);
	~ScreenObject() {};

	glm::vec2 getPosition();
	SDL_Rect getSourceRect();
	SDL_Rect getDestRect();


protected:
	glm::vec2 position, startPosition;
	glm::vec2 size;
	SDL_Rect source;
	

};