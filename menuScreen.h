#pragma once

#include <glm/glm.hpp>
#include <iostream>

#include "WindowHandler.h"
#include "ScreenObject.h"
#include "globals.h"

class menuScreen
{
public:
	menuScreen();

	bool update(glm::vec2 mPos);

	ScreenObject* getStartButton();
	ScreenObject* getQuitButton();
private:
	ScreenObject startButton, quitButton;
};