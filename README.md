# Grade: B
Liked:
  The additional features.
Disliked:
  Some things could be named better ("getChangeTheFuckingThing()")
  Some of the stuff in the Level class really does not belong there. (Pathing, collision...)
  Not actually doing collision detection but rather checking if the location you want to move into is occupied.
  Would have liked some comments more comments.

# Assignment 1

## Group creation deadline 2016/9/30 23:59:59
## Hand in deadline 2016/10/7 23:59:59

In this assignment you will be making Pac Man.  
This is a group assignment. You will make groups of 3 students (if for some reason this is not possible contact me before the group creation deadline). One of the group members must send an e-mail to me at johannes.hovland2@ntnu.no with the name of the group members by the group creation deadline.

One of the group members will have to **fork**(not clone) this repo. You will be developing on that fork.  
Remember to give Simon and me access so that we can look at what you have done.

## Required work
1. Finish the code in ```Level::createWalls()``` and ```WindowHandler::draw(ScreenObject* object)```so that walls are added to the level and can be drawn. The walls do not need to have any fancy textures. Filling an appropriate space with color will do.
2. Create a player object.
    1. Use the ```InputHandler``` to read input from **w, a, s, d** and add movement events to the event queue.
    1. Pop events from the event queue in the update function (_main.cpp_) and use them to move Pac Man around.
    1. Implement collision detection so that Pac Man cannot pass through the walls and can pick up orbs.
    1. Load the provided sprite sheet and animate Pac Man as he moves
5. Create and display objects representing orbs/crates/fruit that can be collected by Pac Man and gives him points. (Sprites/textures optional but it must be visible.) These objects should disapear as Pac Man collects them.
6. Modify the mapfile so that it includes data about where spheres that give points should be placed.
5. Create a text handling class.
    1. Use the text handling class to load the font provided.
    1. Use the font to display a score in the top left corner of the screen as Pac Man picks up orbs.
1. Update this document. (See botom.)

## Restrictions
1. No drawing to screen outside of the ```WindowHandler``` (You are free to modify it as much as you want.)
2. You **must** use the SDL2 library for the graphics.
3. Do not use SDL_TTF for the font handling.

## Suggestions for additional work
1. Add enemies (Colliding with the enemies resets Pac Man to the start position.)
    1. Give the enemies a simple AI.
2. Add functionality that let Pac Man exit on one side of the screen and enter on the other.
3. Add sound.
4. Add more levels and a way to switch between them.
5. Implement proper kerning in the font.
6. Etc

##Group comments
###Who are the members of the group?###
Glenn Adrian Nordhus,
Christoffer Berg Kongsness,
Benjamin Gordon Wendling
###What did you implement and how did you do it? (Individually)###
Most of the work done was done in groups, but as an individual I added a simple menu screen, as well as some random small stuff, and fixes here and there. To add the menu screen I added an if check in the main file that checked if the player had pressed play before the program went on with it's usual business! -Benjamin

I'm not quite sure what I can take full credit for or not, but I did quite a bit of work on the collision as well as some of the basic stuff individually! All of the rest was done as a group. To do the collision I checked the next tile the player was heading into, and what type of tile it was, if it just so happened to be a ghost, a wall, or a pickup respective functions and/or velocity change happened. -Glenn

I did orb handling, input handling and some of the ghost AI alone also cleaned up alot of the code. 
All the rest was done as a group. we added a* to the ghost and kept all of them after PoKman so to 
stop that i added that the ghost should focus on a point in their corner every 7 sec so that the ghost would not all go the same way. -Christoffer

###What parts if any of the base code did you change and why?###
We fixed the bug related to the nextframe timer, as it was not functioning properly/not functioning at all. Also we edited a whole bunch of shit, but we don't remember quite what it was............
###What was the hardest part of this assignment? (Individually)###
Collision -Glenn

Collision -Benjamin

Collision and Astar - Christoffer


###Did you feel like the assignment was an appropriate ammount of work? (Individually)###
I felt like it was an appropriate ammount of work -Benjamin

Only if we get an A -Glenn

what glenn said -Christoffer
###Other comments###