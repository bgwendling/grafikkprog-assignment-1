#include <stdio.h>
#include <SDL2/SDL.h>
#include <string>
#include <iostream>
#include <SDL2/SDL_mixer.h>

#include "InputHandler.h"
#include "WindowHandler.h"
#include "globals.h"
#include "GameEvent.h"
#include "ScreenObject.h"
#include "Level.h"
#include "menuScreen.h"

//Creates a Level object for each line in gLEVELS_FILE andplace it it the gLevels vector.
//Those lines are paths of files with map data.
//See Level::loadMap for more information.
//Returns a pointer to the first Level object (currentLevel).
Level* loadLevels() {
	FILE* file = fopen(gLEVELS_FILE, "r");

	int f;
	std::string tempString;

	fscanf(file, "Number of files: %d", &f);

	for(int i = 1; i<=f; i++) {
		char tempCString[51];
		fscanf(file, "%50s", tempCString);
		tempString = tempCString;
		Level level(tempString);
		gLevels.push_back(level);
	}

	fclose(file);
	file = nullptr;
	
	return &gLevels.front();
}

//Initializes the InputHandler and WindowHandler
//The WindowHandler intitilizes SDL
//Loads levels and returns a pointer to the first Level by calling loadLevels()
Level* init() {
	Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096);
	InputHandler::getInstance().init();
	if(!WindowHandler::getInstance().init()) {
		gRunning = false;
	}

	return loadLevels();
}

//While there are events in the eventQueue. Process those events. 
void update(float deltaTime, std::queue<GameEvent>& eventQueue, Level* currentLevel) {
	GameEvent nextEvent;
	glm::vec2 dir;

	while(!eventQueue.empty()) {
		nextEvent = eventQueue.front();
		eventQueue.pop();

		if(nextEvent.action == ActionEnum::PLAYER_MOVE_UP)
		{
			currentLevel->getPlayer()->direction(glm::vec2(0,-1));
		}
		if(nextEvent.action == ActionEnum::PLAYER_MOVE_DOWN)
		{
			currentLevel->getPlayer()->direction(glm::vec2(0,1));
		}
		if(nextEvent.action == ActionEnum::PLAYER_MOVE_LEFT)
		{
			currentLevel->getPlayer()->direction(glm::vec2(-1,0));
		}
		if(nextEvent.action == ActionEnum::PLAYER_MOVE_RIGHT)
		{
			currentLevel->getPlayer()->direction(glm::vec2(1,0));
		}
	}
	
	currentLevel->update(deltaTime);

}

bool update(std::queue<GameEvent>& eventQueue, menuScreen menu)
{
	GameEvent nextEvent;

	while(!eventQueue.empty()) {
		nextEvent = eventQueue.front();
		eventQueue.pop();

		if(nextEvent.action == ActionEnum::MOUSECLICKLEFT)
		{
			//glm::vec2 mPos = ;
			return(menu.update(InputHandler::getInstance().getMousePosition()));
		}
	}
	return true;
}

//This is the main draw function of the program. All calls to the WindowHandler for drawing purposes should originate here.
void draw(Level* currentLevel) {
	WindowHandler::getInstance().clear();
	WindowHandler::getInstance().drawList(currentLevel->getWalls());
	WindowHandler::getInstance().drawList(currentLevel->getOrbs());
	WindowHandler::getInstance().drawList(currentLevel->getGhosts());
	WindowHandler::getInstance().drawList(currentLevel->getPowerUps());
	WindowHandler::getInstance().draw(currentLevel->getPlayer());
	WindowHandler::getInstance().drawText();
	WindowHandler::getInstance().update();
}

void draw(menuScreen menu)
{
	WindowHandler::getInstance().clear();
	WindowHandler::getInstance().draw(menu.getStartButton());
	WindowHandler::getInstance().draw(menu.getQuitButton());
	WindowHandler::getInstance().update();
}

//Calls cleanup code on program exit.
void close() {
	WindowHandler::getInstance().close();
}
void nextLevel(Level* currentLevel)
{

}

int main(int argc, char *argv[]) {
	std::queue<GameEvent> eventQueue; //Main event queue for the program.
	Level* currentLevel = nullptr;
	float nextFrame = 1/gFpsGoal; //Time between frames in seconds
	float nextFrameTimer = 0.0f; //Time from last frame in seconds
	float deltaTime = 0.0f; //Time since last pass through of the game loop.
	auto clockStart = std::chrono::high_resolution_clock::now(); //Clock used for timing purposes
	auto clockStop = clockStart;
	uint level = 0;
	currentLevel = init();

	menuScreen mainMenu;
	bool menu = true;

	while(gRunning) 
	{

		clockStart = std::chrono::high_resolution_clock::now();

		
		InputHandler::getInstance().readInput(eventQueue);
		
		if(menu)
		{
			menu = update(eventQueue, mainMenu);
			
			if(nextFrameTimer >= nextFrame) {

				draw(mainMenu);

				nextFrameTimer -= nextFrame;
			}
		}

		else
		{	
			update(deltaTime, eventQueue, currentLevel);
	
			if(currentLevel->nextLevel())
			{
				level++;
				if (gLevels.size() <= level)
				{
					gLevels.clear();
			 		currentLevel = loadLevels();
					level = 0;
				}
				else
				{
					currentLevel = &gLevels[level];
				}
			}
			if (currentLevel->hit())
			{
				gLevels.clear();
				currentLevel = loadLevels();
				WindowHandler::getInstance().reset();
			}
			if(nextFrameTimer >= nextFrame) {

				draw(currentLevel);

				nextFrameTimer -= nextFrame;
			}
		}

		clockStop = std::chrono::high_resolution_clock::now();
		deltaTime = std::chrono::duration<float, std::chrono::seconds::period>(clockStop - clockStart).count();

		nextFrameTimer += deltaTime;
		
	}

	close();
	return 0;
}